.PHONY: clean

CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG -o3 

ASMFLAGS=-felf64 -g

BUILDDIR=build
SRCDIR=src
CC=gcc
NASM = nasm
EXECUTABLE=image-transfomer

	
all: $(BUILDDIR)/bmpfile.o $(BUILDDIR)/util.o $(BUILDDIR)/transform.o $(BUILDDIR)/main.o $(BUILDDIR)/sepia.o $(BUILDDIR)/sepia_asm.o 
	$(CC) -o $(BUILDDIR)/$(EXECUTABLE) $^
	

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/bmpfile.o: $(SRCDIR)/bmpfile.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/transform.o: $(SRCDIR)/transform.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@
	
$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@
	
$(BUILDDIR)/sepia.o: $(SRCDIR)/sepia.c build
	$(CC) -c $(CFLAGS) $< -o $@
	
$(BUILDDIR)/sepia_asm.o: $(SRCDIR)/sepia_asm.asm build
	$(NASM) $(ASMFLAGS) $(SRCDIR)/sepia_asm.asm -o $(BUILDDIR)/sepia_asm.o
	
	

clean:
	rm -rf $(BUILDDIR)



