
section .text

%define SSE_XXM_REGISTER_SIZE 16

global sepia_one_sse

; rdi = pixvalues_xmm0_2, rsi = coeffs_xmm3_5
sepia_one_sse:

	movdqa xmm0, [rdi]; move Aligned Double Quadword
	movdqa xmm1, [rdi+SSE_XXM_REGISTER_SIZE]
	movdqa xmm2, [rdi+(SSE_XXM_REGISTER_SIZE*2)]

	mulps xmm0, [rsi]  ;  ->xmm0, multiply Packed Single-Precision Floating-Point Values
	mulps xmm1, [rsi+SSE_XXM_REGISTER_SIZE]  ;  ->xmm1
	mulps xmm2, [rsi+(SSE_XXM_REGISTER_SIZE*2)] ;  ->xmm2
	
	addps xmm0, xmm1; add Packed Single-Precision Floating-Point Values
	addps xmm0, xmm2


    cvtps2dq xmm0, xmm0 ; convert 4 floats into 4 signed doublewords (int)
    packssdw xmm0, xmm0 ; convert 4 signed doublewords into 4 signed words (using only first half of xmm0) with saturation
    packuswb xmm0, xmm0 ; convert 4 signed words into 4 unsigned bytes (using only first quarter of xmm0) with saturation
    movd [rdi], xmm0 ; coping first quarter of xmm0 back to memory 

ret

