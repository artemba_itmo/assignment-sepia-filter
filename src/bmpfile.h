#ifndef BMPFILE_H
#define BMPFILE_H

#include  <stdio.h>
#include  <stdint.h>
#include "transform.h"


#ifdef _MSC_VER 
#pragma pack(push, 1)
#endif


struct bmp_file_header
{
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
}
#ifndef _MSC_VER  
	__attribute__((packed))
#endif
;


struct bmp_header
{
	struct bmp_file_header file_header;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
}
#ifndef _MSC_VER  
	__attribute__((packed))
#endif
;

#ifdef _MSC_VER  
#pragma pack(pop)
#endif



enum error_status from_bmp(FILE* in, struct bmp_header *bmp_hdr, struct image* img);


enum error_status to_bmp(FILE* out, struct bmp_header *external_bmp_hdr, struct image const* img, int32_t ortogonal_rotation_flag);




#endif



