#include "bmpfile.h"
#include "rawimage.h"
#include "transform.h"

#ifdef _MSC_VER 
	#include <io.h>
#else
	#include <unistd.h>
#endif



enum error_status from_bmp( FILE* in, struct bmp_header *bmp_hdr, struct image* img)
{
	uint64_t rc, buffer_offset;
	int32_t y;
	//int32_t filehandler = _fileno(in);

	//move file pointer to the beggining of the file (security sake, considering that open and read operations are separated)
	if( fseek(in, 0, SEEK_SET) < 0 )
		return READ_SKIP_ERROR;


	// read bmp file header
	if( fread(bmp_hdr,sizeof(struct bmp_header),1,in) != 1 )
		return READ_DATA_ERROR;

	//BMP-file type "BM"
	if (bmp_hdr->file_header.bfType != 0x4D42)
		return READ_INVALID_SIGNATURE;

	//24-bit BMP (RGB)
	if(bmp_hdr->biBitCount != 24 )
		return READ_INVALID_BITS;

	//dimensions
	if (!bmp_hdr->biWidth || !bmp_hdr->biHeight )
		return READ_INVALID_HEADER;

	// fill image structure
	img->height = bmp_hdr->biHeight;
	img->width = bmp_hdr->biWidth;
	if ( (rc = alloc_image_buffer(img)) != OP_OK) //allocate image->data buffer here (image dimensions are known and checked)
		return rc;

	//calculate x padding (bmp's x dimension must be multipe of 4 - IN BYTES!)
	uint64_t line_len = img->width * sizeof(struct pixel);
	uint64_t line_padding = line_len % 4;
	if (line_padding)
		line_padding = 4 - line_padding;

	
	// read bmp file image by lines considering eventual padding (lines in bmp file are mirrored vertically!)
	for (y = (int32_t)img->height-1, buffer_offset= (uint64_t)y*img->width;// points to the begginig of each new line in img buffer 
		 y >= 0; 
		 y--, buffer_offset-= img->width)
	{
		if (fread((void *)(img->data+ buffer_offset), (unsigned int)line_len,1,in) != 1 )
		{
			free_image_buffer(img);
			return READ_DATA_ERROR;
		}
		if (line_padding)
		{
			if (fseek(in, (long)line_padding, SEEK_CUR) < 0)//move file pointer for line_padding bytes ahead from the current position
			{
				free_image_buffer(img);
				return READ_SKIP_ERROR;
			}
		}
	}

	return OP_OK;
}



enum error_status to_bmp( FILE* out, struct bmp_header *external_bmp_hdr, struct image const* img, int32_t ortogonal_rotation_flag)
{
	struct bmp_header *bmp_hdr,internal_bmp_hdr;
	if (external_bmp_hdr)
	{
		bmp_hdr = external_bmp_hdr;
		if (ortogonal_rotation_flag)
		{
			uint32_t exchange = bmp_hdr->biXPelsPerMeter;
			bmp_hdr->biXPelsPerMeter = bmp_hdr->biYPelsPerMeter;
			bmp_hdr->biYPelsPerMeter = exchange;
		}
	}
	else
	{
		internal_bmp_hdr.file_header.bfType = 0x4d42; // BFT_BITMAP "BM"
		internal_bmp_hdr.file_header.bOffBits = sizeof(struct bmp_header);
		internal_bmp_hdr.file_header.bfReserved = 0;

		internal_bmp_hdr.biPlanes = 1;
		internal_bmp_hdr.biBitCount = 24;// 3-bytes per pixel RGB
		internal_bmp_hdr.biSize = sizeof(struct bmp_header)-sizeof(struct bmp_file_header);

		internal_bmp_hdr.biCompression = 0;// BI_RGB;

		internal_bmp_hdr.biXPelsPerMeter = 0;
		internal_bmp_hdr.biYPelsPerMeter = 0;
		internal_bmp_hdr.biClrUsed = 0;
		internal_bmp_hdr.biClrImportant = 0;

		bmp_hdr = &internal_bmp_hdr;
	}


	uint64_t buffer_offset;
	int32_t y;
	int8_t  padding_array[4-1] = {0,0,0};// max padding is 4-1 len

	//calculate x padding (bmp's x dimension must be multipe of 4 - IN BYTES!)
	uint64_t line_len = img->width * sizeof(struct pixel);
	uint64_t line_padding = line_len % 4;
	if (line_padding)
		line_padding = 4 - line_padding;

	//modify only some fields of bmp_header thus preserving original information
	bmp_hdr->file_header.bfileSize = (uint32_t)(bmp_hdr->file_header.bOffBits + (line_len + line_padding)*img->height);

	bmp_hdr->biWidth =  (uint32_t)img->width;
	bmp_hdr->biHeight = (uint32_t)img->height;
	bmp_hdr->biSizeImage = (uint32_t)((line_len + line_padding)*img->height);


	//move file pointer to the beggining of the file (security sake, considering that open and write operations are separated)
	if( fseek(out, 0, SEEK_SET) < 0 )
		return WRITE_ERROR;

	// read bmp file header
	if (fwrite(bmp_hdr, sizeof(struct bmp_header),1,out) != 1)
		return WRITE_ERROR;

	// write bmp file image by lines considering eventual padding (must be mirrored vertically!)
	for (y = (int32_t)(img->height - 1), buffer_offset = y*img->width;// points to the begginig of each new line in img buffer 
		 y >= 0;
		 y--, buffer_offset -= img->width)
	{
		if (fwrite( (void *)(img->data + buffer_offset), (unsigned int)line_len,1 ,out) != 1 )
			return WRITE_ERROR;

		if (line_padding)
		{
			if (fwrite( (void *)(padding_array), (unsigned int)line_padding,1,out) != 1 )
				return WRITE_ERROR;
		}
	}

	return OP_OK;
}




