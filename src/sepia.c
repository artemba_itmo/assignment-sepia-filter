#include "rawimage.h"
#include <inttypes.h>


#include <memory.h>



//struct pixel { uint8_t b, g, r; };
//struct image {
//	uint32_t width, height;
//	struct pixel* array;
//};
static unsigned char sat(uint64_t x) {
	if (x < 256) 
		return (unsigned char)x; 
	return 255;
}

static void sepia_one(struct pixel* const pixel) {
	static const float c[3][3] = {
		{ .393f, .769f, .189f },
		{ .349f, .686f, .168f },
		{ .272f, .543f, .131f } };
	struct pixel const old = *pixel;
	pixel->r = sat((uint64_t)(	old.r * c[0][0] + old.g * c[0][1] + old.b * c[0][2])	);
	pixel->g = sat((uint64_t)(	old.r * c[1][0] + old.g * c[1][1] + old.b * c[1][2])	);
	pixel->b = sat((uint64_t)( 	old.r * c[2][0] + old.g * c[2][1] + old.b * c[2][2]) 	);
}
void sepia_c_inplace(struct image* img) {
	uint32_t x, y;
	for (y = 0; y < img->height; y++)
		for (x = 0; x < img->width; x++)
			sepia_one(img->data + y*img->width + x);
}


#ifndef _MSC_VER
void sepia_one_sse(float const *pixvalues_xmm0_2, float const *coeffs_xmm3_5);
#endif

#define SSE_BLOCK_PIXEL_NUMBER 4 // 4 pixels * 3 bytes (b,g,r) = 12 elements; 
#define SSE_BLOCK_ELEMENT_NUMBER (SSE_BLOCK_PIXEL_NUMBER*sizeof(struct pixel)) // 4 pixels * 3 bytes (b,g,r) = 12 elements; 
#define SSE_BLOCK_ELEMENT_NUMBER_FLOAT (SSE_BLOCK_ELEMENT_NUMBER*sizeof(float)) // 12 elemnents * 4 bytes (b,g,r) = 48 bytes;
#define SSE_XXM_REGISTER_SIZE  16
#define SSE_BLOCK_ITERATION_NUMBER  (SSE_BLOCK_ELEMENT_NUMBER_FLOAT/SSE_XXM_REGISTER_SIZE) // 3
#define SSE_MEM_ALIGN	16
#define SSE_ELEMENTS_IN_ITERATION 4

#define c11 .131f
#define c12 .543f
#define c13 .272f

#define c21 .168f 
#define c22 .686f
#define c23 .349f

#define c31 .189f
#define c32 .769f
#define c33 .393f

#ifdef _MSC_VER
__declspec(align(SSE_MEM_ALIGN))
#endif
float pixvalues_xmm0_2[SSE_BLOCK_ELEMENT_NUMBER]
#ifndef _MSC_VER
__attribute__((aligned(16)))
#endif
;

//B = b*c11 + g*c12 + r*c13
//G = b*c21 + g*c22 + r*c23
//R = b*c31 + g*c32 + r*c33

// coeffs for 3 iterations
#ifdef _MSC_VER
__declspec(align(SSE_MEM_ALIGN))
#endif
//for iteration 1 ( b1g1r1b2 ) : xmm0 = b1b1b1b2, xmm1 = g1g1g1g2, xmm2 = r1r1r1r2;
float coeffs_xmm3_5_iteration_1[SSE_BLOCK_ELEMENT_NUMBER]
#ifndef _MSC_VER
__attribute__((aligned(16)))
#endif
= {//   b1,  g1,  r1,  b2 
	c11, c21, c31, c11,		//xmm3 
	c12, c22, c32, c12,		//xmm4 
	c13, c23, c33, c13 };	//xmm5 



#ifdef _MSC_VER
__declspec(align(SSE_MEM_ALIGN))
#endif
//  for iteration 2 ( g2r2b3g3 ) : xmm0 =g2g2g3g3, xmm1 = r2r2r3r3, xmm2 = b2b2b3b3; 																							  
float coeffs_xmm3_5_iteration_2[SSE_BLOCK_ELEMENT_NUMBER] 
#ifndef _MSC_VER
__attribute__((aligned(16)))
#endif
= {//   g2,  r2,  b3,  g3 
	c21, c31, c11, c21,		//xmm3 
	c22, c32, c12, c22,		//xmm4 
	c23, c33, c13, c23 };	//xmm5 


#ifdef _MSC_VER
__declspec(align(SSE_MEM_ALIGN))
#endif
//  for iteration 3 ( r3b4g4r4 ) : xmm0 =r3r4r4r4, xmm1 = b3b4b4b4, xmm2 = g3g4g4g4; 			
float coeffs_xmm3_5_iteration_3[SSE_BLOCK_ELEMENT_NUMBER] 
#ifndef _MSC_VER
__attribute__((aligned(16)))
#endif
= {//   r3,  b4,  g4,  r4 
	c31, c11, c21, c31,		//xmm3 
	c32, c12, c22, c32,		//xmm4 
	c33, c13, c23, c33 };	//xmm5 


#ifdef _MSC_VER
__declspec(align(SSE_MEM_ALIGN)) float coeffs_xmm3_5[SSE_BLOCK_ELEMENT_NUMBER];
#else
static float *coeffs_xmm3_5 __attribute__((aligned(16)));
#endif

static const float byte_to_float[256] = {
0.000000f, 1.000000f, 2.000000f, 3.000000f, 4.000000f, 5.000000f, 6.000000f, 7.000000f, 8.000000f, 9.000000f,
10.000000f, 11.000000f, 12.000000f, 13.000000f, 14.000000f, 15.000000f, 16.000000f, 17.000000f, 18.000000f,
19.000000f, 20.000000f, 21.000000f, 22.000000f, 23.000000f, 24.000000f, 25.000000f, 26.000000f, 27.000000f,
28.000000f, 29.000000f, 30.000000f, 31.000000f, 32.000000f, 33.000000f, 34.000000f, 35.000000f, 36.000000f,
37.000000f, 38.000000f, 39.000000f, 40.000000f, 41.000000f, 42.000000f, 43.000000f, 44.000000f, 45.000000f,
46.000000f, 47.000000f, 48.000000f, 49.000000f, 50.000000f, 51.000000f, 52.000000f, 53.000000f, 54.000000f,
55.000000f, 56.000000f, 57.000000f, 58.000000f, 59.000000f, 60.000000f, 61.000000f, 62.000000f, 63.000000f,
64.000000f, 65.000000f, 66.000000f, 67.000000f, 68.000000f, 69.000000f, 70.000000f, 71.000000f, 72.000000f,
73.000000f, 74.000000f, 75.000000f, 76.000000f, 77.000000f, 78.000000f, 79.000000f, 80.000000f, 81.000000f,
82.000000f, 83.000000f, 84.000000f, 85.000000f, 86.000000f, 87.000000f, 88.000000f, 89.000000f, 90.000000f,
91.000000f, 92.000000f, 93.000000f, 94.000000f, 95.000000f, 96.000000f, 97.000000f, 98.000000f, 99.000000f,
100.000000f, 101.000000f, 102.000000f, 103.000000f, 104.000000f, 105.000000f, 106.000000f, 107.000000f,
108.000000f, 109.000000f, 110.000000f, 111.000000f, 112.000000f, 113.000000f, 114.000000f, 115.000000f,
116.000000f, 117.000000f, 118.000000f, 119.000000f, 120.000000f, 121.000000f, 122.000000f, 123.000000f,
124.000000f, 125.000000f, 126.000000f, 127.000000f, 128.000000f, 129.000000f, 130.000000f, 131.000000f,
132.000000f, 133.000000f, 134.000000f, 135.000000f, 136.000000f, 137.000000f, 138.000000f, 139.000000f,
140.000000f, 141.000000f, 142.000000f, 143.000000f, 144.000000f, 145.000000f, 146.000000f, 147.000000f,
148.000000f, 149.000000f, 150.000000f, 151.000000f, 152.000000f, 153.000000f, 154.000000f, 155.000000f,
156.000000f, 157.000000f, 158.000000f, 159.000000f, 160.000000f, 161.000000f, 162.000000f, 163.000000f,
164.000000f, 165.000000f, 166.000000f, 167.000000f, 168.000000f, 169.000000f, 170.000000f, 171.000000f,
172.000000f, 173.000000f, 174.000000f, 175.000000f, 176.000000f, 177.000000f, 178.000000f, 179.000000f,
180.000000f, 181.000000f, 182.000000f, 183.000000f, 184.000000f, 185.000000f, 186.000000f, 187.000000f,
188.000000f, 189.000000f, 190.000000f, 191.000000f, 192.000000f, 193.000000f, 194.000000f, 195.000000f,
196.000000f, 197.000000f, 198.000000f, 199.000000f, 200.000000f, 201.000000f, 202.000000f, 203.000000f,
204.000000f, 205.000000f, 206.000000f, 207.000000f, 208.000000f, 209.000000f, 210.000000f, 211.000000f,
212.000000f, 213.000000f, 214.000000f, 215.000000f, 216.000000f, 217.000000f, 218.000000f, 219.000000f,
220.000000f, 221.000000f, 222.000000f, 223.000000f, 224.000000f, 225.000000f, 226.000000f, 227.000000f,
228.000000f, 229.000000f, 230.000000f, 231.000000f, 232.000000f, 233.000000f, 234.000000f, 235.000000f,
236.000000f, 237.000000f, 238.000000f, 239.000000f, 240.000000f, 241.000000f, 242.000000f, 243.000000f,
244.000000f, 245.000000f, 246.000000f, 247.000000f, 248.000000f, 249.000000f, 250.000000f, 251.000000f,
252.000000f, 253.000000f, 254.000000f, 255.000000f };



void sepia_asm_inplace(struct image* img) {
	size_t i,iteration;
	struct pixel *cur_pixel_block_ptr, cur_pixel_block_buffer[SSE_BLOCK_PIXEL_NUMBER];
	uint32_t *dest_pixel_ptr;

	for (i = 0; i + SSE_BLOCK_PIXEL_NUMBER <= (size_t) img->height * img->width; i+= SSE_BLOCK_PIXEL_NUMBER)
	{
		// process element quartets (4 consecutive bytes) with SSE ASM routine, by 3 iterations: b1g1r1b2 g2r2b3g3 r3b4g4r4 …
		cur_pixel_block_ptr = (img->data + i);
		memcpy(cur_pixel_block_buffer, cur_pixel_block_ptr, sizeof(cur_pixel_block_buffer));// make a copy of ORIGINAL pixels to be used within iterations
			
		for (iteration = 1;	iteration <= SSE_BLOCK_ITERATION_NUMBER; iteration++)
		{
			//prepare pixvalues_xmm0_2:
			if (iteration == 1) //  for iteration 1 ( b1g1r1b2 ) : xmm0 = b1b1b1b2, xmm1 = g1g1g1g2, xmm2 = r1r1r1r2; 
			{
				pixvalues_xmm0_2[0] = pixvalues_xmm0_2[1]= pixvalues_xmm0_2[2]=byte_to_float[cur_pixel_block_buffer[0].b];
				pixvalues_xmm0_2[3] = byte_to_float[cur_pixel_block_buffer[1].b];

				//xmm_block_index+= 4;
				pixvalues_xmm0_2[4] = pixvalues_xmm0_2[4 + 1] = pixvalues_xmm0_2[4 + 2] = byte_to_float[cur_pixel_block_buffer[0].g];
				pixvalues_xmm0_2[4 + 3] = byte_to_float[cur_pixel_block_buffer[1].g];

				//xmm_block_index += 4;
				pixvalues_xmm0_2[8] = pixvalues_xmm0_2[8 + 1] = pixvalues_xmm0_2[8 + 2] = byte_to_float[cur_pixel_block_buffer[0].r];
				pixvalues_xmm0_2[8 + 3] = byte_to_float[cur_pixel_block_buffer[1].r];

				#ifdef _MSC_VER
				memcpy(coeffs_xmm3_5, coeffs_xmm3_5_iteration_1, SSE_BLOCK_ELEMENT_NUMBER_FLOAT);
				#else
				coeffs_xmm3_5 = coeffs_xmm3_5_iteration_1;
				#endif

				dest_pixel_ptr = (uint32_t*)&cur_pixel_block_ptr[0].b;

			}
			else
			if (iteration == 2) //  for iteration 2 ( g2r2b3g3 ) : xmm0 =b2b2b3b3, xmm1 = g2g2g3g3, xmm2 = r2r2r3r3; 
			{
				pixvalues_xmm0_2[0] = pixvalues_xmm0_2[1]= byte_to_float[cur_pixel_block_buffer[1].b];
				pixvalues_xmm0_2[2] = pixvalues_xmm0_2[3] = byte_to_float[cur_pixel_block_buffer[2].b];

				pixvalues_xmm0_2[4] = pixvalues_xmm0_2[4 + 1] =  byte_to_float[cur_pixel_block_buffer[1].g];
				pixvalues_xmm0_2[4 + 2] = pixvalues_xmm0_2[4 + 3] = byte_to_float[cur_pixel_block_buffer[2].g];

				pixvalues_xmm0_2[8] = pixvalues_xmm0_2[8 + 1] =  byte_to_float[cur_pixel_block_buffer[1].r];
				pixvalues_xmm0_2[8 + 2] = pixvalues_xmm0_2[8 + 3] = byte_to_float[cur_pixel_block_buffer[2].r];

#ifdef _MSC_VER
				memcpy(coeffs_xmm3_5, coeffs_xmm3_5_iteration_2, SSE_BLOCK_ELEMENT_NUMBER_FLOAT);
#else
				coeffs_xmm3_5 = coeffs_xmm3_5_iteration_2;
#endif
				dest_pixel_ptr = (uint32_t*)&cur_pixel_block_ptr[1].g;
			}
			else
			if (iteration == 3) //  for iteration 3 ( r3b4g4r4 ) : xmm0 =b3b4b4b4, xmm1 = g3g4g4g4, xmm2 = r3r4r4r4; 
			{
				pixvalues_xmm0_2[0] = byte_to_float[cur_pixel_block_buffer[2].b];
				pixvalues_xmm0_2[1] = pixvalues_xmm0_2[2] = pixvalues_xmm0_2[3] = byte_to_float[cur_pixel_block_buffer[3].b];

				pixvalues_xmm0_2[4] =  byte_to_float[cur_pixel_block_buffer[2].g];
				pixvalues_xmm0_2[4 + 1] = pixvalues_xmm0_2[4 + 2] = pixvalues_xmm0_2[4 + 3] = byte_to_float[cur_pixel_block_buffer[3].g];

				pixvalues_xmm0_2[8] = byte_to_float[cur_pixel_block_buffer[2].r];
				pixvalues_xmm0_2[8 + 1] = pixvalues_xmm0_2[8 + 2] = pixvalues_xmm0_2[8 + 3] = byte_to_float[cur_pixel_block_buffer[3].r];

#ifdef _MSC_VER
				memcpy(coeffs_xmm3_5, coeffs_xmm3_5_iteration_3, SSE_BLOCK_ELEMENT_NUMBER_FLOAT);
#else
				coeffs_xmm3_5 = coeffs_xmm3_5_iteration_3;
#endif
				dest_pixel_ptr = (uint32_t*)&cur_pixel_block_ptr[2].r;
			}

			// SSE calculations for whole block
			#ifdef _MSC_VER
			__asm {

				movdqa xmm0, [pixvalues_xmm0_2]
				movdqa xmm1, [pixvalues_xmm0_2 + SSE_XXM_REGISTER_SIZE]
				movdqa xmm2, [pixvalues_xmm0_2 + (SSE_XXM_REGISTER_SIZE * 2)]

				mulps xmm0, [coeffs_xmm3_5];->xmm0
				mulps xmm1, [coeffs_xmm3_5 + SSE_XXM_REGISTER_SIZE];->xmm1
				mulps xmm2, [coeffs_xmm3_5 + (SSE_XXM_REGISTER_SIZE * 2)];->xmm2

				addps xmm0, xmm1
				addps xmm0, xmm2

				cvtps2dq xmm0, xmm0
				packssdw xmm0, xmm0
				packuswb xmm0, xmm0

				movd [pixvalues_xmm0_2], xmm0
			}
			#else
			sepia_one_sse(pixvalues_xmm0_2, coeffs_xmm3_5);
			#endif

			//copy calculated values back to the image buffer - pixels are already prepared by sepia_one_sse 
			*dest_pixel_ptr = *((uint32_t*)pixvalues_xmm0_2);
		}

	}
	// remaining pixels process with C-function
	for (; i < img->height*img->width; i++)
		sepia_one(img->data + i);

}