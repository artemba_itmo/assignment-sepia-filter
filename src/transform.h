#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "rawimage.h"
#include "util.h"

enum transformations {
	ROTATION_NONE = 0,
	ROTATION_90CCW,
	ROTATION_90CW,
	ROTATION_180,
	SEPIA_NONE,
	SEPIA_C,
	SEPIA_ASM_SSE
};
struct image rotate(struct image const source_img, int32_t rotation_direction);
void sepia_c_inplace(struct image const *img);
void sepia_asm_inplace(struct image const *img);

int32_t alloc_image_buffer(struct image *imgptr);

void free_image_buffer(struct image *imgptr);

#endif
