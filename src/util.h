#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdint.h>


#ifndef _MSC_VER
#include "timeb.h" //VER4200
#define _timeb timeb
#define _ftime ftime
#else
#include <sys\timeb.h>
#endif



/*  deserializer   */
enum error_status {
	OP_OK = 0,
		OPEN_READ_ERROR,
		READ_INVALID_SIGNATURE,
		READ_INVALID_BITS,
		READ_INVALID_HEADER,
		READ_DATA_ERROR,
		READ_SKIP_ERROR,
		OPEN_WRITE_ERROR,
		WRITE_ERROR,
		MEMORY_ALLOC_ERROR
	/* коды других ошибок  */
};

enum error_status open_binary_file_to_read( char *filename, FILE** file);

enum error_status open_binary_file_to_write( char *filename, FILE** file);

void  print_msg(FILE *out_stream, char *message, char *argument_string, int32_t *argument_int);

char * convert_err_code_to_string(int32_t const argument_int);

//#ifdef _MSC_VER 
int64_t   DiffTimeInMilliseconds(const struct _timeb *T1, const struct _timeb *T2);
//#endif

#ifndef _MSC_VER 
#define sprintf_s snprintf
#endif

#endif



