#include <stdbool.h>
#include <stdlib.h>

#ifndef _MSC_VER
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdint.h>

#include "util.h"
#include "bmpfile.h"
#include "rawimage.h"
#include "transform.h"


int main(int argc, char **argv) 
{
	int32_t rc, rotation_direction, sepia_mode;

	if (argc < 3 || argc > 5 )
	{
		#define ARGUMENTS_STR_SIZE 200
		char arg_str[ARGUMENTS_STR_SIZE];
		sprintf_s(arg_str, ARGUMENTS_STR_SIZE,"\n<source-image.bmp> <transformed-image.bmp> [<rotation-direction {%d(NONE),%d(90CCW),%d(90CW),%d(180)}>] [<sepia-filter {%d(C),%d(ASM-SSE)}>]",
												ROTATION_NONE, ROTATION_90CCW, ROTATION_90CW, ROTATION_180, 
												SEPIA_C,SEPIA_ASM_SSE);
		print_msg(stderr,"command line arguments format", arg_str,NULL);
		return 1;
	}
	// decode [<rotation-direction {%d(NONE),%d(90CCW),%d(90CW),%d(180)}>] [<sepia-filter {%d(C),%d(ASM-SSE)}>]
	rotation_direction = ROTATION_90CCW;//default rotation mode
	sepia_mode = SEPIA_NONE;//default sepia mode
	if (argc >= 4 || argc <= 5 )
	{
		rc= atoi(argv[3]);
		if (rc >= ROTATION_NONE && rc <= ROTATION_180)
			rotation_direction = rc;
		else
		if (rc >= SEPIA_NONE && rc <= SEPIA_ASM_SSE)
			sepia_mode = rc;
		else
		{
			print_msg(stderr, "Error in the FIRST command line attribute", "Unknown value", &rc);
			return 1;
		}

		if (argc == 5)
		{
			rc= atoi(argv[4]);
			if (rc >= ROTATION_NONE && rc <= ROTATION_180)
				rotation_direction = rc;
			else
			if (rc >= SEPIA_NONE && rc <= SEPIA_ASM_SSE)
				sepia_mode = rc;
			else
			{
				print_msg(stderr, "Error in the SECOND command line attribute", "Unknown value", &rc);
				return 1;
			}
		}
	}

		

	struct image input_img;
	input_img.data = NULL;// image buffer is not yet allocated

	struct image transformed_img;

	struct bmp_header bmp_hdr;

	FILE	*inputfile, *outputfile;

	if( (rc= open_binary_file_to_read( argv[1], &inputfile)) != OP_OK)
	{
		print_msg(stderr, "Error in open_binary_file_to_read", argv[1], &rc);
		return 1;
	}

	if ((rc = from_bmp(inputfile, &bmp_hdr, &input_img)) != OP_OK)
	{
		fclose(inputfile);
		print_msg(stderr, "Error in from_bmp", NULL, &rc);
		return 1;
	}
	
	transformed_img=rotate(input_img,rotation_direction);
	if(transformed_img.data==NULL)
	{
		fclose(inputfile);
		free_image_buffer(&input_img);
		rc = MEMORY_ALLOC_ERROR;
		print_msg(stderr, "Error in rotate", NULL, &rc);
		return 1;
	}

	if (sepia_mode != SEPIA_NONE)
	{

		#ifndef _MSC_VER
		struct rusage r;
		struct timeval start;
		struct timeval end;
		getrusage(RUSAGE_SELF, &r);
		start = r.ru_utime;
		#else
		struct _timeb t1, t2;
		_ftime(&t1);
		#endif	

		
		if (sepia_mode == SEPIA_C)
		{
			sepia_c_inplace(&transformed_img);
			printf("Apply SEPIA_C processing\n");
		}
		else
		if (sepia_mode == SEPIA_ASM_SSE)
		{
			sepia_asm_inplace(&transformed_img);
			printf("Apply SEPIA_ASM_SSE processing\n");
		}

#ifndef _MSC_VER
		getrusage(RUSAGE_SELF, &r);
		end = r.ru_utime;
		long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
			end.tv_usec - start.tv_usec;
		printf("\nTime elapsed in microseconds: %ld\n", res);
#else
		_ftime(&t2);
		printf("\nTime elapsed in microseconds: %ld\n", DiffTimeInMilliseconds(&t1, &t2) * 1000);
#endif	

	}

	if( (rc=open_binary_file_to_write( argv[2], &outputfile)) != OP_OK)
	{
		fclose(inputfile);
		free_image_buffer(&input_img);
		free_image_buffer(&transformed_img);
		print_msg(stderr, "Error in open_binary_file_to_write", argv[2], &rc);
		return 1;
	}

	if ((rc = to_bmp(outputfile, &bmp_hdr, &transformed_img, (rotation_direction==ROTATION_90CCW || rotation_direction==ROTATION_90CW) )) != OP_OK)
	{
		fclose(inputfile);
		fclose(outputfile);
		free_image_buffer(&input_img);
		free_image_buffer(&transformed_img);
		print_msg(stderr, "Error in to_bmp", NULL, &rc);
		return 1;
	}

	print_msg(stdout, "done", NULL, NULL);

	fclose(inputfile);
	fclose(outputfile);
	free_image_buffer(&input_img);
	free_image_buffer(&transformed_img);
	return 0;
}
