#include "util.h"


enum error_status open_binary_file_to_read( char *filename, FILE** file)
{

#ifdef _MSC_VER  
	int32_t rc=fopen_s(file,filename, "rb");
	if( rc == 0 )
		return OP_OK;
#else
	*file =fopen(filename, "rb");
	if( (*file) != NULL )
		return OP_OK;
#endif

	
	return OPEN_READ_ERROR;
}


enum error_status open_binary_file_to_write( char *filename, FILE** file)
{
#ifdef _MSC_VER
	int32_t rc = fopen_s(file, filename, "wb");
	if (rc == 0 )
		return OP_OK;
#else
	*file =fopen(filename, "wb");
	if( (*file) != NULL )
		return OP_OK;
#endif
	
	return OPEN_WRITE_ERROR;
}

void  print_msg(FILE *out_stream, char *message, char *argument_string, int32_t *argument_int)
{  
	if(message == NULL)
		return;
#ifdef _MSC_VER 
	char PressEnterKey[] = "\nPress <Enter> key to continue.";
#else
	char PressEnterKey[] = "\n";
#endif	

	#define TMP_STR_SIZE 300
	char str[TMP_STR_SIZE];
	if(argument_string != NULL && argument_int == NULL)
		sprintf_s(str, TMP_STR_SIZE,"%s: %s%s", message, argument_string, PressEnterKey);
	else
	if(argument_string != NULL && argument_int != NULL)
		sprintf_s(str, TMP_STR_SIZE,"%s: %s (%s)%s", message, argument_string, convert_err_code_to_string(*argument_int), PressEnterKey);
	else
	if(argument_string == NULL && argument_int != NULL)
		sprintf_s(str, TMP_STR_SIZE,"%s: (%s)%s", message, convert_err_code_to_string(*argument_int), PressEnterKey);
	else
	//if(argument_string == NULL && argument_int == NULL)
		sprintf_s(str, TMP_STR_SIZE,"%s%s", message, PressEnterKey);
	
		
	fprintf(out_stream, "%s \n",str);
#ifdef _MSC_VER 
	getchar();
#endif	
}

#define ERROR_STR_SIZE 50
static char error_string[ERROR_STR_SIZE];
char * convert_err_code_to_string(int32_t const argument_int)
{

	switch(argument_int)
	{
		case OPEN_READ_ERROR:
			sprintf_s(error_string, ERROR_STR_SIZE, "Open file for reading");
			break;
		case READ_INVALID_SIGNATURE:
			sprintf_s(error_string, ERROR_STR_SIZE, "Invalid BMP file signature");
			break;
		case READ_INVALID_BITS:
			sprintf_s(error_string, ERROR_STR_SIZE, "BMP file BitCount not supported");
			break;
		case READ_INVALID_HEADER:
			sprintf_s(error_string, ERROR_STR_SIZE, "BMP file header");
			break;
		case READ_DATA_ERROR:
			sprintf_s(error_string, ERROR_STR_SIZE, "Reading data");
			break;
		case READ_SKIP_ERROR:
			sprintf_s(error_string, ERROR_STR_SIZE, "File seek operation");
			break;
		case OPEN_WRITE_ERROR:
			sprintf_s(error_string, ERROR_STR_SIZE, "Open file for writing");
			break;
		case WRITE_ERROR:
			sprintf_s(error_string, ERROR_STR_SIZE, "Writing data");
			break;
		case MEMORY_ALLOC_ERROR:
			sprintf_s(error_string, ERROR_STR_SIZE, "memory allocation");
			break;
		default:
			sprintf_s(error_string, ERROR_STR_SIZE, "Unknown code %d", argument_int);
			break;
	}

	return error_string;
}


//#ifdef _MSC_VER 
int64_t   DiffTimeInMilliseconds(const struct _timeb *T1, const struct _timeb *T2)
{
	int64_t  sec, ms;

	sec = (int64_t)(T2->time - T1->time);
	ms = T2->millitm - T1->millitm;
	if (ms < 0)
	{
		sec--;
		ms += 1000;
	}
	if (sec < 0)
		sec += 60;

	return sec * 1000 + ms;
}
//#endif





